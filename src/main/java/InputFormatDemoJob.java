import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Demonstration of custom input format
 */
public class InputFormatDemoJob extends Configured implements Tool {
    @Override
    public int run(String[] args) throws Exception {
        Job job = GetJobConf(getConf(), args[0], args[1]);
        return job.waitForCompletion(true) ? 0 : 1;
    }

    static public class DemoMapper extends Mapper<NullWritable, Text, Text, IntWritable> {
        static final Pattern word_expr = Pattern.compile("\\p{L}+");
        static final IntWritable one = new IntWritable(1);


        @Override
        protected void map(NullWritable key, Text value, Context context) throws IOException, InterruptedException {
            HashSet<String> uniq_words = new HashSet<>();
            Matcher matcher = word_expr.matcher(value.toString());

            while (matcher.find()) {
                String word = matcher.group().toLowerCase();
                if (uniq_words.add(word))
                    context.write(new Text(word), one);
            }
        }
    }

    static public class DemoReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        @Override
        protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int total = 0;
            for (IntWritable val: values) {
                total += val.get();
            }
            context.write(key, new IntWritable(total));
        }
    }

    public static Job GetJobConf(Configuration conf, String input, String out_dir) throws IOException {
        Job job = Job.getInstance(conf);
        job.setJarByClass(InputFormatDemoJob.class);
        job.setJobName(InputFormatDemoJob.class.getCanonicalName());

        job.setInputFormatClass(IndexedGzipInputFormat.class);
        FileInputFormat.addInputPath(job, new Path(input));
        FileOutputFormat.setOutputPath(job, new Path(out_dir));

        job.setMapperClass(DemoMapper.class);
        job.setCombinerClass(DemoReducer.class);
        job.setReducerClass(DemoReducer.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        return job;
    }

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new InputFormatDemoJob(), args);
        System.exit(exitCode);
    }
}
