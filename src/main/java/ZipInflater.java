import java.io.ByteArrayOutputStream;
import java.util.zip.Inflater;


public class ZipInflater {
    byte[] buf = new byte[4096];

    public byte[] Decompress(byte[] data) throws java.util.zip.DataFormatException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        Inflater inflater = new Inflater();
        int n;

        inflater.setInput(data, 0, data.length);
        while ((n = inflater.inflate(buf)) > 0)
            output.write(buf, 0, n);
        inflater.end();

        return output.toByteArray();
    }
}
