import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IOUtils;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class IndexedGzipInputFormat extends FileInputFormat<NullWritable, Text> {
    static public class ChunkedPartsSplit extends FileSplit {
        private ArrayList<Integer> page_lens;

        public ChunkedPartsSplit() { super(); }

        public ChunkedPartsSplit(Path path, long offset, long length, ArrayList<Integer> parts) {
            super(path, offset, length, new String[] {});
            page_lens = new ArrayList<>(parts);
        }

        @Override
        public void write(DataOutput out) throws IOException {
            super.write(out);
            out.writeInt(page_lens.size());
            for (int x: page_lens) {
                out.writeInt(x);
            }
        }

        @Override
        public void readFields(DataInput in) throws IOException {
            super.readFields(in);
            int n = in.readInt();
            page_lens = new ArrayList<>();

            while(n-- != 0) {
                page_lens.add(in.readInt());
            }
        }

        ArrayList<Integer> getPageLengths() {
            return page_lens;
        }
    }

    public class GzipBlockRecordReader extends RecordReader<NullWritable, Text> {
        ArrayList<Integer> parts;
        Text value = new Text();
        FSDataInputStream input;
        ZipInflater inflater = new ZipInflater();
        int cur_part = 0;

        @Override
        public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
            ChunkedPartsSplit chunked_split = (ChunkedPartsSplit)split;

            Path file = chunked_split.getPath();
            FileSystem fs = file.getFileSystem(context.getConfiguration());

            input = fs.open(file);
            input.seek(chunked_split.getStart());

            parts = chunked_split.getPageLengths();
        }

        @Override
        public boolean nextKeyValue() throws IOException, InterruptedException {
            if (cur_part >= parts.size())
                return false;

            int len = parts.get(cur_part++);
            byte[] buf = new byte[len];
            IOUtils.readFully(input, buf, 0, len);

            try {
                value.set(inflater.Decompress(buf));
            }
            catch(java.util.zip.DataFormatException e) {
                throw new IOException("Failed to inflate buffer");
            }

            return true;
        }


        @Override
        public NullWritable getCurrentKey() throws IOException, InterruptedException {
            return NullWritable.get();
        }

        @Override
        public Text getCurrentValue() throws IOException, InterruptedException {
            return value;
        }

        @Override
        public float getProgress() throws IOException, InterruptedException {
            return (float)cur_part / parts.size();
        }

        @Override
        public void close() throws IOException {
            IOUtils.closeStream(input);
        }
    }

    public static final String BYTES_PER_MAP = "mapreduce.input.indexedgz.bytespermap";
    @Override
    public List<InputSplit> getSplits(JobContext context) throws IOException {
        List<InputSplit> splits = new ArrayList<>();
        for (FileStatus status: listStatus(context)) {
            splits.addAll(getPlainDataSplits(status, context.getConfiguration()));
        }

        return splits;
    }

    @Override
    public RecordReader<NullWritable, Text> createRecordReader(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
        GzipBlockRecordReader reader = new GzipBlockRecordReader();
        reader.initialize(split, context);
        return reader;
    }

    protected List<InputSplit> getPlainDataSplits(FileStatus status, Configuration conf) throws IOException {
        Path pdpath = status.getPath();
        Path idxpath = pdpath.suffix(".idx");
        FileSystem fs = idxpath.getFileSystem(conf);

        FSDataInputStream in = null;
        try {
            in = fs.open(idxpath);
            return splitByIndex(pdpath, in, getNumBytesPerSplit(conf));
        }
        finally {
            IOUtils.closeStream(in);
        }
    }

    protected List<InputSplit> splitByIndex(Path fileName, DataInputStream in, long split_bytes) throws IOException {
        List<InputSplit> splits = new ArrayList<>();
        ArrayList<Integer> parts = new ArrayList<>();
        long length = 0;
        long prev_end = 0;


        while(true)
        {
            int rec_len;
            try {
                rec_len = Integer.reverseBytes(in.readInt());
            }
            catch(EOFException e) {
                break;
            }

            length += rec_len;
            parts.add(rec_len);

            if (length - prev_end >= split_bytes) {
                splits.add(makeSplitVerbose(fileName, prev_end, length - prev_end, parts));
                parts.clear();
                prev_end = length;
            }
        }

        if (!parts.isEmpty()) {
            splits.add(makeSplitVerbose(fileName, prev_end, length - prev_end, parts));
        }

        return splits;
    }

    protected ChunkedPartsSplit makeSplitVerbose(Path path, long offset, long length, ArrayList<Integer> parts) {
        System.out.printf("Adding split: %s; %d bytes from offset %d [%d docs]\n",
                path.getName(), length, offset, parts.size());
        return new ChunkedPartsSplit(path, offset, length, parts);
    }

    public static long getNumBytesPerSplit(Configuration conf) {
        return conf.getLong(BYTES_PER_MAP, 33554432);
    }

}
