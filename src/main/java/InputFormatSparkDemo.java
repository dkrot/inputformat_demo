import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import scala.Tuple2;

import java.io.IOException;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputFormatSparkDemo {
    private String infiles;
    private String outdir;

    InputFormatSparkDemo(String infiles, String outdir) {
        this.infiles = infiles;
        this.outdir = outdir;
    }

    JavaSparkContext getSparkContext() {
        SparkConf conf = new SparkConf().setAppName(InputFormatSparkDemo.class.getCanonicalName());
        return new JavaSparkContext(conf);
    }

    static class UniqDocWords implements FlatMapFunction<Text, String> {
        static final Pattern word_expr = Pattern.compile("\\p{L}+");

        @Override
        public Iterable<String> call(Text value) throws Exception {
            HashSet<String> uniq_words = new HashSet<>();
            Matcher matcher = word_expr.matcher(value.toString());

            while (matcher.find()) {
                String word = matcher.group().toLowerCase();
                uniq_words.add(word);
            }

            return uniq_words;
        }
    }


    void run() throws IOException {
        JavaSparkContext sc = getSparkContext();
        Configuration conf = sc.hadoopConfiguration();
        Job job = Job.getInstance(conf);
        FileInputFormat.addInputPath(job, new Path(infiles));

        sc.newAPIHadoopRDD(job.getConfiguration(), IndexedGzipInputFormat.class, NullWritable.class, Text.class)
                .values()
                .flatMap(new UniqDocWords())
                .mapToPair(word -> new Tuple2<>(word, 1))
                .reduceByKey((a, b) -> a + b)
                .saveAsTextFile(outdir);
    }

    public static void main(String[] args) throws Exception {
        new InputFormatSparkDemo(args[0], args[1]).run();
    }
}
