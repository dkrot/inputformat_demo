#!/usr/bin/env python

import zlib
import struct
import sys

with open(sys.argv[1], 'rb') as fd:
    sz = struct.unpack('i', fd.read(4))[0]
    print 'Document size: %d' % sz
    buf = fd.read(sz)
    print zlib.decompress(buf)
