#!/usr/bin/env python

import document_pb2
import struct
import argparse
import gzip
import uuid
import zlib
import sys


class IndexedFileWriter:
    def __init__(self, path):
        self.out = open(path, 'wb')
        self.out_idx = open(path + '.idx', 'wb')
        self.total_size = 0

    @property
    def size(self):
        return self.total_size;

    def add(self, data):
        self.out.write(data)
        self.out_idx.write(struct.pack('i', len(data)))
        self.total_size += len(data)

    def flush(self):
        self.out.flush()
        self.out_idx.flush()


class IndexedCollectionBlockWriter:
    def __init__(self, path_expr, max_size):
        self.path_expr = path_expr
        self.max_size = max_size
        self.idxfile = None

    def add(self, url, content):
        if not self.idxfile or self.idxfile.size >= self.max_size:
            self.flush()
            fpath = self.path_expr % uuid.uuid4()
            self.idxfile = IndexedFileWriter(fpath)

        self.idxfile.add(zlib.compress(content.encode('utf-8')))

    def flush(self):
        if self.idxfile:
            self.idxfile.flush()
        

class DocumentStreamReader:
    def __init__(self, paths):
        self.paths = paths

    def open_single(self, path):
        return gzip.open(path, 'rb') if path.endswith('.gz') else open(path, 'rb')

    def __iter__(self):
        for path in self.paths:
            with self.open_single(path) as stream:
                while True:
                    sb = stream.read(4)
                    if sb == '':
                        break

                    size = struct.unpack('i', sb)[0]
                    msg = stream.read(size)
                    doc = document_pb2.document()
                    doc.ParseFromString(msg)
                    yield doc


def parse_command_line():
    parser = argparse.ArgumentParser(description='document converter')
    parser.add_argument('-s', '--size', metavar='max-size', type=int, required=False, default=32*2**20, help='maximum block size')
    parser.add_argument('-o', '--output', metavar='pattern',type=str, required=True, help='pattern in form /path/to/file-%%s.ext')
    parser.add_argument('files', nargs='+', help='Input files (.gz or plain) to process')
    return parser.parse_args()


def main():
    opts = parse_command_line()
    reader = DocumentStreamReader(opts.files)
    pack_writer = IndexedCollectionBlockWriter(opts.output, opts.size)

    for doc in reader:
        print "%s\t%d bytes" % (doc.url, len(doc.text) if doc.HasField('text') else -1)
        if doc.HasField('text'):
            pack_writer.add(doc.url, doc.text)

    pack_writer.flush()


if __name__ == '__main__':
    main()
